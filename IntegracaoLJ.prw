#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} IntegracaoLJ
Integração com LJ Integrada
@author  Samuel Dantas
@since   10/02/2022
@version 1.0
@Redmine 
/*/
//-------------------------------------------------------------------
Class IntegracaoLJ
    DATA cUrlRaiz
    DATA cVerb
    DATA cUrl
    DATA cParams
    DATA cBody
    DATA cStsCod
    DATA cStsRes
    DATA dPrazo
    DATA aHeadOut
    DATA cEndpoint
    DATA cServCod
    DATA cAccessToken
    DATA oURL
    DATA oClient
    DATA cResult
    DATA oRestClient
    DATA playload
    DATA formFields
    DATA cMsgErro
    DATA cHTTPCode
    DATA cID
    DATA oTempJson
    METHOD NEW() constructor
    METHOD POSTPARAMS()
    METHOD setPARAMS(cVerb, cParams, cBody)
    METHOD setPath(cUrl)
    METHOD setVerb(cVerb)
    METHOD getBody()
    METHOD getUrl()
    METHOD getResult()
    METHOD getHTTPCode()
    METHOD getMsgErro()
    METHOD getProcessId()
    METHOD getObjJson()

EndClass

//Inicializa classe já com os dados genéricos.
METHOD NEW(cUrl) Class IntegracaoLJ
    Default cUrl        := ''    
    self:cUrlRaiz       := Alltrim(SuperGetMv("MS_LJIURL",.F., "https://api.awsli.com.br" ))
    self:cUrl           := self:cUrlRaiz + cUrl
    self:cEndpoint      := cUrl
    self:cVerb          := ''
    self:cParams        := ''
    self:cBody          := ''
    self:cStsCod        := ''
    self:cAccessToken   := ''
    self:cStsRes        := ''    
    self:aHeadOut       := {}
    self:playload       := ''
    self:formFields     := ''
    self:cResult     := ''

Return Self

//Faz a conexão com serviço rest
METHOD POSTPARAMS() Class IntegracaoLJ
    Local lRet := .F.
    Local cJsonResult := ''    
    Local cHeaderRet := ''    
    Local aHead := aClone(self:aHeadOut)
    Local cBody := EncodeUTF8(self:cBody)
    self:cMsgErro := ''
    
   If Empty(self:cEndPoint)
        self:cMsgErro := 'URL (EndPoint) não definida. Utilize o método setPath(cUrl).' 
        Return .F.
    EndIf

    self:oRestClient := FWRest():New(self:cUrlRaiz) 
    self:oRestClient:setPath(self:cEndpoint)

    If self:cVerb == 'GET'
        lRet := self:oRestClient:Get(aHead, cBody)
    ElseIf self:cVerb == 'POST'
        self:oRestClient:setPostParams(cBody)
        lRet := self:oRestClient:Post(aHead)
    ElseIf self:cVerb == 'PUT'
        lRet := self:oRestClient:Put(aHead, cBody)
    ElseIf self:cVerb == 'PATCH'
        self:cResult := HTTPQuote( self:cUrlRaiz+self:cEndPoint, "PATCH", self:cParams, cBody, 120, aHead, @cHeaderRet )
    ElseIf self:cVerb == 'DELETE'
        lRet := self:oRestClient:Delete(aHead, cBody)
    Else
        self:cMsgErro := 'Operação não está no escopo de verbos de envios válidos.'        
    EndIf

    self:cHTTPCode := Iif(Empty(self:cMsgErro), self:oRestClient:GetHTTPCode(), '')
    If !Empty(self:cHTTPCode)
        cJsonResult := DecodeUTF8(self:oRestClient:GetResult())       
        self:cResult   := cJsonResult
        self:oTempJson := JsonObject():new()
        self:oTempJson:FromJson(cJsonResult)
    ElseIf self:cVerb == 'PATCH'
        self:cHTTPCode := Iif(self:cVerb == 'PATCH', SUBsTR(cHeaderRet,10,3), self:cHTTPCode )
    EndIf

Return lRet

METHOD setPARAMS(cVerb, cParams, cBody) Class IntegracaoLJ
    // Local cChave        := AllTrim(SuperGetMv("MS_CHVJLI", .F., "chave_api 5d901203a507f7435049 aplicacao d42a1949-81de-4ef7-8564-e22e9b1fa319"))
    Local cChave        := AllTrim(SuperGetMv("MS_CHVJLI", .F., "chave_api 7578b8e8eb98c8591877 aplicacao b123aabe-3ae9-484f-ab97-a8a74525277c"))//PRODUCAO
    Default cParams     := ''
    Default cBody       := ''

    //Local oClient
    self:cID     := ''
    self:cMsgErro:= ''
    self:cVerb   := cVerb    
    self:cBody   := cBody
    self:cParams := cParams
    
    cMsg := 'Authorization: '+Alltrim(cChave)+''  

    self:aHeadOut := { ;
        "accept: */*",;
        "Content-Type: application/json",;                        
        cMsg;
    }
    
Return

METHOD getURL() Class IntegracaoLJ
Return self:cUrl

METHOD getBody() Class IntegracaoLJ
Return self:cBody

//Seta novo 
METHOD setVerb(cVerb) Class IntegracaoLJ
    self:cVerb := cVerb
Return

METHOD setPath(cUrl) Class IntegracaoLJ
    self:cEndPoint := cUrl
    self:cUrl   := self:cUrlRaiz + cUrl    
Return

METHOD getResult() Class IntegracaoLJ
Return DecodeUTF8(Iif(Empty(self:cHTTPCode), self:oRestClient:GetLastError(), self:oRestClient:GetResult()))

METHOD getHTTPCode() Class IntegracaoLJ
Return self:cHTTPCode

METHOD getMsgErro() Class IntegracaoLJ
Return self:cMsgErro

METHOD getProcessId() Class IntegracaoLJ
Return self:cID

METHOD getObjJson() Class IntegracaoLJ
Return self:oTempJson
