#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TRYEXCEPTION.CH"

#DEFINE ENTER CHR(13) + CHR(10)

#DEFINE P_FD 1	// Field
#DEFINE P_JS 2	// Json
#DEFINE P_OB 3	// Obrigatorio
#DEFINE P_TP 4	// Tipo

Static _aStruct := {}

/*/{Protheus.doc} CPagto
	Controle de pagamentos
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	/*/
Class CPagto
		
	Data cErr	
	Data nParcelas	
	Data aPagto
	Data nVlrParc				

	Method New() Constructor
	
	Method FromJson(cPagto)
	Method GetValue(cField, xRet)
		
EndClass

/*/{Protheus.doc} New
	Construtor
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method New() Class CPagto	
				
	::cErr		:= ""	
	::nParcelas	:= 1
	::nVlrParc	:= 0
	::aPagto	    := {}
	
Return

/*/{Protheus.doc} FromJson
	Conversao para objeto
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method FromJson(jObject) Class CPagto
		
	Local aStruct		:= {}	
	Local nX			:= 0
    Local cForma        := ""
				
	Private jPagto	    := JsonObject():New()
	Private cField		:= ""

	Default jObject  	:= ""    

	// Efetua a conversao
	jPagto := jObject

	IF LoadStruct(@aStruct) == .F.
		::cErr := "[CPAGTO] Falha ao carregar a estrutura."
        Return .F.
	Else
		::aPagto := {}	
		For nX := 1 To Len(aStruct)
		
			cField 	:= ""
			cType	:= ""
			xValue 	:= Nil

			IF Empty(aStruct[nX][P_JS]) == .F.
				cField 	:= 'jPagto["'+aStruct[nX][P_JS]+'"]'
				cType	:= Type(cField)
				xValue 	:= &cField
				IF cType == "U" .And. aStruct[nX][P_OB]
					::cErr := "[CPAGTO] Campo obrigatorio nao informado: [" + aStruct[nX][P_JS] + "]"
					Return .F.			
				EndIF
			EndIF            
            
			IF aStruct[nX][P_TP] == "N" .AND. ValType(xValue) == "C"
				xValue := val(StrTran(xValue,'.',''))
			EndIF

            IF aStruct[nX][P_FD] == "L4_FORMA"
                IF PayToProt(xValue, @cForma, jPagto) == .F.
                    ::cErr := "[CPAGTO] Forma de pagamento nao esperada: [" + xValue + "]"
				    Return .F.	
                EndIF
                xValue := cForma
			ElseIF aStruct[nX][P_FD] == "L4_VALOR"
				xValue := xValue/100
            Else
                xValue := &cField
                IF aStruct[nX][P_TP] == "D"
                    xValue := CToD(xValue)
                ElseIF aStruct[nX][P_TP] == "C"
                    xValue := AllTrim(xValue)
                EndIF	
            EndIF	           
			::nParcelas := jPagto['parcelamento']['numero_parcelas']
			::nVlrParc  := jPagto['parcelamento']['valor_parcela']
            Aadd(::aPagto, {aStruct[nX][P_FD], xValue})	
		Next nX			
		Return .T.
	EndIF	

Return .F.

/*/{Protheus.doc} GetValue
	(long_description)
	@author Leonardo F. Bulcao - UserFunction
	@since 26/04/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method GetValue(cField, xRet) Class CPagto
		
	Local nP 		:= 0

	Default cField 	:= ""
	Default xRet	:= Nil

	IF Empty(::aPagto)
		::cErr	:= "[CPAGTO] Dados de cadastro nao informados"
        Return .F.
	EndIF	

	IF (nP := Ascan(::aPagto, {|x| x[1] == cField})) > 0
		Return ::aPagto[nP][2]
	EndIF

Return xRet

/*/{Protheus.doc} LoadStruct
	Montagem da estrutura
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Static Function LoadStruct(aStruct)

	Local lRet			:= .T.
	Local aAreaSX3		:= SX3->(GetArea())
	Local nX			:= 0
	
	Default aStruct		:= {}

	// Caso ja esteja populado
	IF Empty(_aStruct) == .F.
		aStruct := Aclone(_aStruct)
		Return .T.
	EndIF
	
	// Field Protheus, Propriedade Mobile, Tipo	
	Aadd(aStruct, {"L4_VALOR"	, "valor_pago"	        , .T.})
	Aadd(aStruct, {"L4_FORMA"	, "pagamento_tipo"	    , .T.})	
	//Aadd(aStruct, {"L4_DATA"	, "data"	        , .F.})	
	
	SX3->(DbSetOrder(2))
	For nX := 1 To Len(aStruct)
		IF SX3->(DbSeek(aStruct[nX][P_FD]))
			Aadd(aStruct[nX], SX3->X3_TIPO)
		EndIF
	Next nX

	SX3->(RestArea(aAreaSX3))

	//Salva na variavel estatica
	_aStruct := Aclone(aStruct)	

Return lRet

/*/{Protheus.doc} PayToProt
    Converte as formas de pagamento para Protheus
    @type  Static Function
    @author Leonardo F. Bulcao - UserFunction
    @since 25/05/2021
    @version version
    @param param_name, param_type, param_descr
    @return return_var, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
    /*/
Static Function PayToProt(cPaySys, cForma, jPagto)
    
    Local aPaySystem    := {}
    Local nP            := 0

    Default cPaySys     := ""
    Default cForma      := ""
	if jPagto['pagamento_tipo'] = 'creditCard'
		cForma := "CC" 
	else
		cForma := "R$" 
	endIf
	
	Return .T.

    // Formas de pagamento
    Aadd(aPaySystem, {"6", "BO"})

    IF (nP := Ascan(aPaySystem, {|x| x[1] == cPaySys})) > 0
        cForma := aPaySystem[nP][2]
        Return .T.
    EndIF

Return .F.

