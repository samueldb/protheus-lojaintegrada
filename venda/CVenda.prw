#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TRYEXCEPTION.CH"

#DEFINE ENTER CHR(13) + CHR(10)

#DEFINE P_FD 1	// Field
#DEFINE P_JS 2	// Json
#DEFINE P_OB 3	// Obrigatorio
#DEFINE P_TP 4	// Tipo

Static _aStruct := {}

/*/{Protheus.doc} CVenda
	Classe de venda
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	/*/
Class CVenda
			
	Data cNumOrc
	Data cNumSite	
	Data cErr	
	Data aVenda
	Data oCliente	
	Data aItens
	Data aPagtos    
	Data dEmissao	
	Data pedido

	Method New() Constructor	
	Method FromJson(cVenda)
	Method Save()
	Method GetValue(cField, xRet)
	Method GeraSLR()	
	Method GeraSL4()
    Method Valida()	
    Method getTipoEnvio()	
			
EndClass

/*/{Protheus.doc} New
	Construtor
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method New() Class CVenda	
		
	::cNumOrc	:= ""
	::cNumSite	:= ""	
	::cErr		:= ""	
	::aVenda 	:= {}
	::oCliente	:= Nil	
	::aItens		:= {}
	::aPagtos	:= {}   
	::dEmissao	:= CToD("  /  /  ")
		
Return

/*/{Protheus.doc} FromJson
	Conversao para objeto
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method FromJson(jObject) Class CVenda
		
	Local aStruct		:= {}
	Local cField		:= ""
    Local nP,nI,nX      := 0	
	Local oVendaItem
	Local jVendaItem 	
	Local oPagto	
       				
	Private jVenda		:= Nil	
	Private jPackAtt	:= Nil
	Private jPackage	:= Nil
	Private jPgtoData	:= Nil
	Private jTransacts	:= Nil
	Private jPayment	:= Nil

	Default jObject		:= JsonObject():New()

	// Efetua a conversao	
	jVenda := jObject
	::pedido := jVenda
	IF LoadStruct(@aStruct) == .F.
		::cErr := "[CVENDA] Falha ao carregar a estrutura."
		Return .F.	
	Else
		::aVenda := {}	
		For nX := 1 To Len(aStruct)
		
			cField 	:= 'jVenda["' + aStruct[nX][P_JS] + '"]'
			cType 	:= Type(cField)            
			
			IF cType == "U" .And. aStruct[nX][P_OB]
				::cErr := "[CVENDA] Campo obrigatorio nao informado: [" + aStruct[nX][P_JS] + "]"
				Return .F.
            EndIF           
			
			xValue := &cField

			IF aStruct[nX][P_TP] == "N" .AND. valType(xValue) == 'C'
				xValue := val(xValue)
			ElseIf aStruct[nX][P_TP] == "C" .AND. valType(xValue) == 'N'
				xValue := cValToChar(xValue)
			eNDiF

			IF aStruct[nX][P_FD] == "L1_XNMSITE" 
				::cNumSite := xValue
			ElseIF aStruct[nX][P_FD] == "L1_EMISSAO"        // "2021-05-19T12:24:45.2936910+00:00"                
                xValue := SToD(StrTran(Left(xValue, 10), "-", ""))
				::dEmissao := xValue
            ElseIF aStruct[nX][P_FD] == "L1_HORA"       	// "2021-05-19T12:24:45.2936910+00:00"                
                xValue := Substr(xValue, 12, 8)
            ElseIF aStruct[nX][P_FD] == "L1_DESCONT"                 
                IF (nP := Ascan(xValue, {|x| x["id"] == "Discounts"})) > 0
                    xValue := xValue[nP]["value"]
                    xValue := Abs(xValue)
                Else
                    xValue := 0
                EndIF
                cField := "Discounts"
            ElseIF aStruct[nX][P_FD] == "L1_FRETE"                   
                // IF (nP := Ascan(xValue, {|x| x["id"] == "Shipping"})) > 0
                //     xValue := xValue[nP]["value"]
                //     xValue := Abs(xValue)
				// 	xValue := xValue/100
                // Else
                    // xValue := 0
                // EndIF
                // cField := "Shipping"              
            Else				
				IF aStruct[nX][P_TP] == "D"
					IF Empty(xValue)
						xValue := CToD("  /  /  ")
					Else
						xValue := CToD(xValue)
					EndIF
				ElseIF aStruct[nX][P_TP] == "C"
					xValue := AllTrim(xValue)
				EndIF								
			EndIF	            
			Aadd(::aVenda, {aStruct[nX][P_FD], xValue})
		Next nX	

		
		IF Type('jVenda["cliente_completo"]') <> "U"
			oCliente := CCliente():New()
			IF oCliente:FromJson(jVenda["cliente_completo"], jVenda["endereco_completo"]) == .F.
				::cErr := oCliente:cErr
				Return .F.
			EndIF
			::oCliente := oCliente
        Else
            ::cErr := "[CVENDA] Dados de cliente nao informados: [clientProfileData]"
			Return .F.
		EndIF

		// Itens
		IF Type('jVenda["itens"]') <> "U"
			For nX := 1 To Len(jVenda["itens"])
				jVendaItem := jVenda["itens"][nX]
				oVendaItem := CVendaItem():New()
				IF oVendaItem:FromJson(jVendaItem) == .F.
					::cErr := oVendaItem:cErr
					Return .F.
				EndIF
				Aadd(::aItens, oVendaItem)
			Next nX 
        Else
            ::cErr := "[CVENDA] Itens da venda nao informados: [items]"
			Return .F.
		EndIF
		
		// Pagamentos
		IF Type('jVenda["pagamentos"]') <> "U"
			jPgtoData := jVenda["pagamentos"]
			For nX := 1 To Len(jPgtoData)
				jPayment := jPgtoData[nX]
				oPagto := CPagto():New(::pedido)
				IF oPagto:FromJson(jPayment) == .F.
					::cErr := oPagto:cErr
					Return .F.
				EndIF				
				Aadd(::aPagtos, oPagto)
					
			Next nX
		Else
			::cErr := "[CVENDA] Itens de pagamento nao informados: [paymentData]"
			Return .F.
		EndIF
	EndIF	

Return .T.

/*/{Protheus.doc} Save
	(long_description)
	@author Leonardo F. Bulcao - UserFunction
	@since 26/04/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method Save() Class CVenda		
	Local cErrorBlock := ""
	Local oError := Errorblock({|e| cErrorBlock := e:ErrorStack })// e:Description +
	Private cUFCODLJRS	:= U_SuperGetMv("UF_CODLJRS", .T., "010101", "C", "Codigo da Loja (Campo SLJ->LJ_CODIGO) que deseja efetuar a reserva quando existir item(s) que for do tipo entrega (LR_ENTREGA = 3)")
	Private cMVVENDPAD	:= Alltrim(SuperGetMV("UF_VENDPAD", .F., "000019"))
	Private lMsHelpAuto := .T. //Variavel de controle interno do ExecAuto
	Private lMsErroAuto := .F. //Variavel que informa a ocorr�ncia de erros no ExecAuto
	Private INCLUI := .T. //Variavel necess�ria para o ExecAuto identificar que se trata de uma inclus�o
	Private ALTERA := .F. //Variavel necess�ria para o ExecAuto identificar que se trata de uma inclus�o
	
	Private aSLQ		:= {}
	Private aSLR 		:= {}
	Private aSL4		:= {}
	
	Private nVlrDI		:= 0
	Private nVlrCC		:= 0
	Private nVlrCD		:= 0
	Private nVlrFI		:= 0
	Private nVlrCH		:= 0
	Private nVlrCO		:= 0	
	
	Private nTotLiq		:= 0
	Private nTotBruto	:= 0
	Private nTotMerc	:= 0
	Private nTotDesc	:= 0
	Private nTotPagto	:= 0
	Private nTotFrete	:= 0
	Private lMsErroAuto	:= .F.
	Private INCLUI      := .T. //Variavel necess�ria para o ExecAuto identificar que se trata de uma inclus�o
    Private ALTERA      := .F. 	

	IF Empty(::aVenda)	
		::cErr := "[CVENDA] Dados da venda nao informados"
		Return .F.	
	ElseIF Empty(::aPagtos)	
		::cErr := "[CVENDA] Dados do pagamento nao informados"
		Return .F.
	ElseIF Empty(::aItens)
		::cErr := "[CVENDA] Dados dos itens nao informados"
		Return .F.
	EndIF
			
	// Efetua a gravacao do cliente
	IF ::oCliente:Save() == .F.   
		::cErr := ::oCliente:cErr
		Return .F.
	EndIF	

	// Prepara a SLR
	IF ::GeraSLR() == .F.
		Return .F.
	EndIF	

	// Prepara a SL4
	IF ::GeraSL4() == .F.
		Return .F.
	EndIF	

	// Valor do frete
	nTotFrete 	:= ::GetValue("L1_FRETE", 0)
	nTotBruto 	:= nTotFrete + nTotMerc 
	nTotLiq		:= nTotBruto - nTotDesc	

	// Valida o total pago com o total liquido
	IF nTotPagto <> nTotLiq
		::cErr := "[CVENDA] Existe uma diferenca entre o total pago e o total calculado: "
		::cErr += "[Total Pago R$ " + AllTrim(Transform(Round(nTotPagto, 2), "@E 999,999.99")) + "]"
		::cErr += "[Total Calculado R$ " + AllTrim(Transform(Round(nTotLiq, 2), "@E 999,999.99")) + "]"
		Return .F.
	EndIF	

	DbSelectArea("SA1")
	SA1->(DbSetOrder(1))
	SA1->(DbSeek(xFilial("SA1") + ::oCliente:cCod + ::oCliente:cLoja))
    
	// Preapara a SLQ	
	Aadd(aSLQ, {"LQ_VEND"		, cMVVENDPAD		, NIL})
	Aadd(aSLQ, {"LQ_COMIS"		, 0					, NIL})
	Aadd(aSLQ, {"LQ_CLIENTE"	, SA1->A1_COD		, NIL})
	Aadd(aSLQ, {"LQ_LOJA"		, SA1->A1_LOJA		, NIL})		
	Aadd(aSLQ, {"LQ_TIPOCLI"	, SA1->A1_TIPO		, NIL})
	Aadd(aSLQ, {"LQ_DESCONT"	, 0					, NIL})	
    Aadd(aSLQ, {"LQ_EMISSAO"	, dDatabase			, NIL})
	Aadd(aSLQ, {"LQ_DTLIM"		, dDatabase +	5	, NIL})
	Aadd(aSLQ, {"LQ_CONDPG"		, ""				, NIL})
	Aadd(aSLQ, {"LQ_NUMMOV"		, "1 "				, NIL})
	Aadd(aSLQ, {"LQ_FRETE"		, nTotFrete			, NIL})
	Aadd(aSLQ, {"LQ_RESERVA"	, ""				, NIL})
	Aadd(aSLQ, {"LQ_XNMSITE"	, ::cNumSite		, NIL})
	Aadd(aSLQ, {"LQ_PARCELA"	, GetFormPg(::pedido,'P')			,  NIL })
	Aadd(aSLQ, {"LQ_FORMPG"		, GetFormPg(::pedido,'F')			,  NIL })
	Aadd(aSLQ, {"LQ_OBS"		, CValToChar(::pedido['numero'])	,  NIL })
	// GetFormPg(oPedido,cTipoRet)
	// GetFormPg(oPedido,cTipoRet)
	//Codigo da Loja (Campo SLJ->LJ_CODIGO) que deseja efetuar a reserva quando existir item(s) que for do tipo entrega (LR_ENTREGA = 3)
	//Aadd(aSLQ, {"AUTRESERVA"	, "000001"		, NIL}) 
	Aadd(aSLQ, {"AUTRESERVA"	, cUFCODLJRS	, NIL}) 
	// AjustaEst(aSLR, .T.)
	
	Begin Sequence
		SetFunName("LOJA701")
		MSExecAuto({|a,b,c,d,e,f,g,h,i,j| Loja701(a,b,c,d,e,f,g,h)}, .F., 3, "", "", {}, aSLQ, aSLR, aSL4, .F., .T.)
		Recover	
	End SEQUENCE

	// AjustaEst(aSLR, .F.)
	
	
	ErrorBlock( oError )
	If !Empty(cErrorBlock)
		lMsErroAuto := .T.
		DisarmTransaction()	
		::cErr := "[CVENDA] "
		::cErr += cErrorBlock
		Return .F.
	ElseIF lMsErroAuto    
		DisarmTransaction()	
		::cErr := "[CVENDA] "
		::cErr += MostraErro("\", "CVenda.log")
		Return .F.
	EndIF   	

Return .T.

/*/{Protheus.doc} GetValue
	(long_description)
	@author Leonardo F. Bulcao - UserFunction
	@since 26/04/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method GetValue(cField, xRet) Class CVenda
		
	Local nP 		:= 0

	Default cField 	:= ""
	Default xRet	:= Nil

	IF Empty(::aVenda)
		::cErr	:= "[CVENDA] Dados de cadastro nao informados"
        Return .F.
	EndIF	

	IF (nP := Ascan(::aVenda, {|x| x[1] == cField})) > 0
		Return ::aVenda[nP][2]
	EndIF

Return xRet

/*/{Protheus.doc} GeraSLR
	Gera os itens da SL2
	@author Leonardo F. Bulcao - UserFunction
	@since 03/05/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method GeraSLR() Class CVenda	
		
	Local nX 		:= 0
	Local cPrdCod	:= ""
	Local cDescri	:= ""
	Local cItem		:= ""
	Local nVlrUnit	:= 0
	Local nVlrTab	:= 0
	Local nVlrDsc	:= 0
	Local nPrcDsc	:= 0
	Local nQtd		:= 0
	Local nVlrItem	:= 0
	Local aRow		:= {}

	DbSelectArea("SB1")
	SB1->(DbSetOrder(1))

	// Prepara a SL2
	For nX := 1 To Len(::aItens)

		aRow 		:= {}
		oVendaItem 	:= ::aItens[nX]
		cPrdCod		:= oVendaItem:GetValue("L2_PRODUTO")

		IF SB1->(DbSeek(xFilial("SB1") + cPrdCod)) == .F.
			::cErr := "[CVENDA] Falha na localizacao do produto: [" + cPrdCod + "]"
			Return .F.
		EndIF

		cDescri		:= SB1->B1_DESC
		cDescri 	:= AllTrim(cDescri)

		cItem 		:= StrZero(nX, TamSX3("L2_ITEM")[1])		
		nVlrUnit	:= oVendaItem:GetValue("L2_VRUNIT"	, 0)
		nVlrTab 	:= oVendaItem:GetValue("L2_PRCTAB"	, 0)
		nVlrDsc 	:= oVendaItem:GetValue("L2_VALDESC"	, 0)	// Valor de Desconto
		nPrcDsc 	:= oVendaItem:GetValue("L2_DESC"	, 0)	// Percentual de Desconto
		nQtd		:= oVendaItem:GetValue("L2_QUANT"	, 0)
		nVlrItem	:= nVlrUnit*nQtd

		Aadd(aRow, {"LR_PRODUTO"	, cPrdCod		, NIL})
		Aadd(aRow, {"LR_VRUNIT"		, nVlrUnit		, NIL})
		Aadd(aRow, {"LR_QUANT"		, nQtd			, NIL})
		Aadd(aRow, {"LR_VLRITEM"	, nVlrItem		, NIL})
		Aadd(aRow, {"LR_UM"			, SB1->B1_UM	, NIL})
		Aadd(aRow, {"LR_DESC"		, nPrcDsc		, NIL})
		Aadd(aRow, {"LR_VALDESC"	, nVlrDsc		, NIL})		
		Aadd(aRow, {"LR_TABELA"		, "001"			, NIL})
		Aadd(aRow, {"LR_DESCPRO"	, 0				, NIL})
		Aadd(aRow, {"LR_VEND"		, cMVVENDPAD	, NIL})
		Aadd(aRow, {"LR_ORIGEM"		, "2"			, NIL})
		
		if ValidaSb2(cPrdCod, nQtd)
			Aadd(aRow, {"LR_ENTREGA"	, "3"			, NIL})
			If SB1->(DbSeek(xFilial('SB1') + PADR(cPrdCod, len(SB1->B1_COD))))
				Aadd(aRow, {"LR_FDTENTR"	, dDataBase + SB1->B1_PE		, NIL})
			else
				Aadd(aRow, {"LR_FDTENTR"	, dDataBase		, NIL})
			EndIf
		Else
			Aadd(aRow, {"LR_ENTREGA"	, "2"			, NIL})
			Aadd(aRow, {"LR_FDTMONT"	, CtoD('')		, NIL})
		EndIf

		
		Aadd(aRow, {"LR_XENTREG"	, ::getTipoEnvio()			, NIL}) // Ato=Ato;Retira Posterior=Retira Posterior;Despacho=Despacho;Entrega=Entrega;Correios=Correios
		
		Aadd(aSLR, aRow)
						
		nTotMerc += (nVlrTab*nQtd)		// Mercadoria sem desconto
		nTotDesc += nVlrDsc		

	Next nX

Return .T.

/*/{Protheus.doc} GeraSL4
	Gera as formas de pagamento
	@author Leonardo F. Bulcao - UserFunction
	@since 03/05/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method GeraSL4() Class CVenda	
	
	Local nX,nR		:= 0
	Local aRow		:= {}
	Local oPagto	:= Nil
	Local cForma	:= ""
	Local nValor 	:= 0
	Local nTot 	:= 0
	Local dData 	:= Nil
	Local nVlrParc	:= 0

	For nX := 1 To Len(::aPagtos)
		
		aRow 	:= {}	
		oPagto 	:= ::aPagtos[nX]

		cForma	:= oPagto:GetValue("L4_FORMA")
		nValor 	:= oPagto:GetValue("L4_VALOR")
		dData	:= Date()
				
		// Caso tenha mais que uma parcela
		IF oPagto:nParcelas > 1

			nVlrParc := oPagto:nVlrParc //(nValor/oPagto:nParcelas)				
		
			For nR := 1 To oPagto:nParcelas
				nTot += nVlrParc
				aRow 	:= {}			
				dData	:= DaySum(dData, 30)				
				
				if nR == oPagto:nParcelas
					if Round(nTot,2) != Round(nValor, 2) //nValor
						nTot := nTot - nVlrParc
						nVlrParc := nValor - nTot
					endIf
				endIf
				
				Aadd(aRow, {"L4_FORMA"	, cForma	, NIL})
				Aadd(aRow, {"L4_DATA"	, dData		, NIL})			
				Aadd(aRow, {"L4_VALOR"	, nVlrParc	, NIL})
				Aadd(aRow, {"L4_ADMINIS", IIF(cForma == "CC","035 - Pagseguro - Cc", ""), NIL})		
				Aadd(aRow, {"L4_NUMCART", " "		, NIL})		
				Aadd(aRow, {"L4_FORMAID", " "		, NIL})		
				Aadd(aRow, {"L4_MOEDA"	, 0			, NIL})		
				Aadd(aRow, {"L4_NSUTEF"	, CValToChar(::pedido['numero'])			, NIL})		
				Aadd(aSL4, aRow)	
			Next nR 
		Else					
			Aadd(aRow, {"L4_FORMA"	, cForma	, NIL})
			Aadd(aRow, {"L4_DATA"	, dData		, NIL})		
			Aadd(aRow, {"L4_VALOR"	, nValor	, NIL})		
			Aadd(aRow, {"L4_ADMINIS", IIF(cForma == "CC","035 - Pagseguro - Cc", "")		, NIL})		
			Aadd(aRow, {"L4_NUMCART", " "		, NIL})		
			Aadd(aRow, {"L4_FORMAID", " "		, NIL})		
			Aadd(aRow, {"L4_MOEDA"	, 0			, NIL})	
			Aadd(aRow, {"L4_NSUTEF"	, CValToChar(::pedido['numero'])			, NIL})	
			Aadd(aSL4, aRow)
		EndIF

		IF cForma == "R$"
			nVlrDI += nValor
		ElseIF cForma == "CC"
			nVlrCC += nValor
		ElseIF cForma == "CD"
			nVlrCD += nValor
		ElseIF cForma == "FI"
			nVlrFI += nValor
		ElseIF cForma == "CH"
			nVlrCH += nValor
		ElseIF cForma == "CO"
			nVlrCO += nValor
		EndIF

		nTotPagto += nValor

	Next nX

Return .T.

/*/{Protheus.doc} Valida
	Valida a existencia do documento
	@author Leonardo F. Bulcao - UserFunction
	@since 03/05/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method Valida() Class CVenda
	
	// Campos informados
	IF Empty(::cNumSite)
		::cErr := "[CVENDA] KEY da da venda informada: [CNUMSITE]"
		Return .F.
	EndIF

	//
    DbSelectArea("SL1")
	SL1->(DbOrderNickName("SL1NUMSITE"))

	// Localiza
    IF SL1->(DbSeek(xFilial("SL1") + ::cNumSite))
		::cErr := "Documento ja gravado: [" + ::cNumSite + "]"
        Return .F.
    EndIF	

Return .T.

/*/{Protheus.doc} LoadStruct
	Montagem da estrutura da SL1
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Static Function LoadStruct(aStruct)

	Local lRet			:= .T.
	Local aAreaSX3		:= SX3->(GetArea())
	Local nX			:= 0
	
	Default aStruct		:= {}

	// Caso ja esteja populado
	IF Empty(aStruct) == .F.
		aStruct := Aclone(aStruct)
		Return .T.
	EndIF	
	
	// Field Protheus, Propriedade Mobile, Tipo		
	Aadd(aStruct, {"L1_XNMSITE"		, "numero"			, .T.})	
	Aadd(aStruct, {"L1_FRETE"		, "valor_envio"		    , .F.})	   
	Aadd(aStruct, {"L1_EMISSAO"		, "data_criacao"	, .T.})	
        		
	SX3->(DbSetOrder(2))
	For nX := 1 To Len(aStruct)
		IF SX3->(DbSeek(aStruct[nX][1]))
			Aadd(aStruct[nX], SX3->X3_TIPO)
		EndIF
	Next nX

	SX3->(RestArea(aAreaSX3))

	//Salva na variavel estatica
	_aStruct := Aclone(aStruct)	

Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} ValidaSb2()
	M�todo repons�vel por fazer um ajuste no estoque 

	@type  Static Function
	@author Samuel Dantas
	@since   21/03/2022
	@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidaSb2(cProduto, nQtd)
	Local aAreaSB2 	:= SB2->(GetArea())
	Local lRet 		:= .F. //Retorna se alterou estoque
	Default lInclui := .F.

	SB2->(DbSetOrder(1))
	If SB2->(DbSeek(xFilial('SB2') + PADR(cProduto, LEN(SB2->B2_COD)) + 'ES' ))
		If SB2->B2_QATU >= nQtd
			lRet := .T.
		EndIf
	EndIf

	SB2->(RestArea(aAreaSB2))

Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} AjustaEst()
	M�todo repons�vel por fazer um ajuste no estoque 

	@type  Static Function
	@author Samuel Dantas
	@since   21/03/2022
	@version 1.0
/*/
//-------------------------------------------------------------------
Static Function AjustaEst(aSLR, lInclui)
	Local aAreaSB2 	:= SB2->(GetArea())
	Local nI 		:= 1
	Local nPosProd  := aScan(aSLR,{|x| AllTrim(x[1]) == 'LR_PRODUTO'})
	Local nPosQtd  	:= aScan(aSLR,{|x| AllTrim(x[1]) == 'LR_QUANT'})
	Local lRet 		:= .F. //Retorna se alterou estoque
	Default lInclui := .F.

	for nI := 1 to len(aSLR)
		//Verifica disponibilidade do produto e quantidade
		nPosProd  := aScan(aSLR[nI],{|x| AllTrim(x[1]) == 'LR_PRODUTO'})
		nPosQtd  := aScan(aSLR[nI],{|x| AllTrim(x[1]) == 'LR_QUANT'})

		cProduto 	:= aSLR[nI][nPosProd][2]
		nQtd 		:= aSLR[nI][nPosQtd][2]

		SB2->(DbSetOrder(1))
		If SB2->(DbSeek(xFilial('SB2') + PADR(cProduto, LEN(SB2->B2_COD)) + 'ES' ))
			
			if lInclui
				If SB2->B2_QATU >= nQtd
					RecLock('SB2', .F.)
						SB2->B2_QATU += nQtd
					SB2->(MsUnLock())
					lRet := .T.
				EndIf
			else
				RecLock('SB2', .F.)
					SB2->B2_QATU -= nQtd
				SB2->(MsUnLock())
			EndIf
		EndIf
	next

	
	
	SB2->(RestArea(aAreaSB2))

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} get
	(long_description)
	@type  Static Function
	@author user
	@since   24/03/2022
	@version 1.0
/*/
//-------------------------------------------------------------------
METHOD getTipoEnvio() Class CVenda

	Local aEnvios 		:= ::pedido['envios']
	Local aPagamentos	:= ::pedido['pagamentos']
	Local nI 			:= 1
	Local cFormaEnvio 	:= 1
	Local cRet 			:= 'Ato'
	// "enviali"
	// "sedex"
	// "pac"
	// "mercadoenvios_normal"
	// "mercadoenvios_expresso"
	// "motoboy"
	// "transportadora"
	// "frenet"
	// "kangu"
	// "mandabem"
	// "melhorenvio"
	// "retirar_pessoalmente"
	
	for nI := 1 to len(aEnvios)
		cFormaEnvio := aEnvios[nI]['forma_envio']['code']

		if Alltrim(cFormaEnvio) = 'retirar_pessoalmente'
			cRet := 'Ato'
		ElseIf Alltrim(cFormaEnvio) = 'pac' .OR. Alltrim(cFormaEnvio) = 'sedex'
			cRet := 'Correios'
		Else
			cRet := 'Entrega'
		endif
	next

	

Return cRet


Static Function GetCondPg(oPedido)

Return

Static Function GetFormPg(oPedido,cTipoRet)
	Local aPagamentos	:= oPedido['pagamentos']
	Local nI 			:= 1
	Local cRet 			:= 'Ato'
	Local cTipoPag		:= ''
	Local nParcelas		:= 1

	for nI := 1 to len(aPagamentos)
		cTipoPag  := aPagamentos[nI]['pagamento_tipo']
		nParcelas := aPagamentos[nI]['parcelamento']['numero_parcelas']

		if cTipoRet == 'F' 
			cRet := 'R$'
			if cTipoPag == 'creditCard'
				cRet := 'CC'
			endIf
		Else 
			cRet := nParcelas
		endif
	next

Return cRet
