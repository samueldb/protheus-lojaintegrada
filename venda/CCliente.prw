#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TRYEXCEPTION.CH"

#DEFINE ENTER CHR(13) + CHR(10)

#DEFINE P_FD 1	// Field
#DEFINE P_JS 2	// Json
#DEFINE P_OB 3	// Obrigatorio
#DEFINE P_TP 4	// Tipo

Static _aStruct := {}

/*/{Protheus.doc} CCliente
	Classe de Cliente
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	/*/
Class CCliente
		
    Data cCGC	
	Data cCod
	Data cLoja
	Data cErr	
	Data aCliente
				
	Method New() Constructor
	Method Save()	
	Method FromJson(cCliente)
	Method GetValue(cField, xRet)
	Method Valida()
		
EndClass

/*/{Protheus.doc} New
	Construtor
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method New() Class CCliente				
	
    ::cCGC		:= ""
	::cCod 		:= ""
	::cLoja 	:= ""
	::cErr		:= ""	
	::aCliente	:= {}
	
Return

/*/{Protheus.doc} Save
	Salvar
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method Save() Class CCliente
	
	Local lNew			:= .T.
	Local aCliente		:= {}
	Local cField		:= ""
	Local xValue		:= ""	
	Local nX			:= 0	
		
	IF Empty(::aCliente)	
		::cErr := "[CCLIENTE] Dados do cliente nao informados"
		Return .F.
	Else	
		// Abre a tabela
		DbSelectArea("SA1")
		SA1->(DbSetOrder(3))

		// Tratamento de ordenacao
		IF Empty(::cCGC)			
			jJsnRet["msg"] := "[CCLIENTE] CGC nao informado"
        	Return .F.  
		ElseIF SA1->(DbSeek(xFilial("SA1") + ::cCGC))
			lNew := .F.		
		EndIF	
		
		Aadd(aCliente, {"A1_FILIAL", xFilial('SA1')})
		// Prepara o array para gravacao de execauto
		For nX := 1 To Len(::aCliente)	
			
			cField := ::aCliente[nX][1]
			xValue := ::aCliente[nX][2]		

			IF cField == "A1_COD" 
				IF lNew
					xValue := GetSxeNum("SA1", "A1_COD")	
					SA1->(ConfirmSX8())
				Else
					xValue := SA1->A1_COD				
				EndIF
				::cCod := xValue
			ElseIF cField == "A1_LOJA" 
				IF lNew
					xValue := "01"					
				Else
					xValue := SA1->A1_LOJA	
				EndIF
				::cLoja := xValue
			ElseIF cField == "A1_NOME"				
				Aadd(aCliente, {"A1_NREDUZ", Left(xValue, TamSX3("A1_NREDUZ")[1])})
			EndIF
			
			Aadd(aCliente, {cField, xValue})
		Next nX			
		
		IF FR271BGeraSL("SA1", aCliente, lNew) == .F.		
			::cErr := "[CCLIENTE] Falha na gravacao do cadastro" 
			Return .F.
		EndIF
	EndIF	

Return .T.

/*/{Protheus.doc} FromJson
	Conversao para objeto
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method FromJson(jObject, jObjectEnd) Class CCliente
	
	Local lRet			:= .F.	
	Local aStruct		:= {}	
	Local nX			:= 0
	Local cCGC			:= ""
			
	Private jCliente
	Private jAddress	
	Private cField		:= ""

	Default jObject		:= JsonObject():New()
	Default jObjectEnd	:= JsonObject():New()

	// Efetua a conversao
	jCliente 	:= jObject
	jAddress	:= jObjectEnd

	IF LoadStruct(@aStruct) == .F.
		::cErr := "[CCLIENTE] Falha ao carregar a estrutura."
	Else
		::aCliente := {}	
		For nX := 1 To Len(aStruct)
		
			cField 	:= ""
			cType 	:= ""
			xValue	:= Nil
			
			IF Empty(aStruct[nX][P_JS]) == .F.
				cField 	:= 'jCliente["'+aStruct[nX][P_JS]+'"]'
				cType 	:= Type(cField)
				xValue	:= &cField
				IF cType == "U" .And. aStruct[nX][P_OB]
					::cErr := "[CCLIENTE] Campo obrigatorio nao informado: [" + aStruct[nX][P_JS] + "]"
					Return .F.			
				EndIF
			EndIF	

			IF aStruct[nX][P_FD] == "A1_PESSOA"
				cCGC := ::GetValue("A1_CGC", "")
				IF Len(cCGC) == 11
					xValue := "F"
				ElseIF Len(cCGC) == 14
					xValue := "J"
				Else
					::cErr := "[CCLIENTE] Falha ao definir o tipo de pessoa com base no documento"
					Return .F.
				EndIF				
			ElseIF aStruct[nX][P_FD] == "A1_NOME"
				xValue := Upper(&cField)
				xValue := xValue
			ElseIF aStruct[nX][P_FD] == "A1_TEL"				
				xValue := &cField
				xValue := Substr(xValue, 3)  		// +5513991997699
			ElseIF aStruct[nX][P_FD] == "A1_DDD"				
				xValue := &cField
				xValue := Substr(xValue, 1, 2)  	// +5513991997699
			ElseIF aStruct[nX][P_FD] == "A1_CGC"
				xValue := &cField
				::cCGC := xValue	
			ElseIF aStruct[nX][P_FD] == "A1_END"
				IF Type('jAddress["endereco"]') == "U"
					::cErr := "[CCLIENTE] Propriedade nao informada: [endereco]"
					Return .F.
				EndIF

				// jAddress := jAddress["endereco"]

				IF Type('jAddress["endereco"]') == "U"
					::cErr := "[CCLIENTE] Propriedade nao informada: [endereco]"
					Return .F.
				EndIF	
				xValue := jAddress["endereco"]		

				IF Type('jAddress["cep"]') == "U"
					::cErr := "[CCLIENTE] Propriedade nao informada: [cep]"
					Return .F.
				EndIF
				Aadd(::aCliente, {"A1_CEP", jAddress["cep"]})

				IF Type('jAddress["cidade"]') == "U"
					::cErr := "[CCLIENTE] Propriedade nao informada: [cidade]"
					Return .F.
				EndIF
				Aadd(::aCliente, {"A1_MUN", jAddress["cidade"]})

				IF Type('jAddress["estado"]') == "U"
					::cErr := "[CCLIENTE] Propriedade nao informada: [estado]"
					Return .F.
				EndIF
				Aadd(::aCliente, {"A1_EST", jAddress["estado"]})

				// IF Type('jAddress["complemento"]') == "U"
				// 	::cErr := "[CCLIENTE] Propriedade nao informada: [complemento]"
				// 	Return .F.
				// EndIF
				Aadd(::aCliente, {"A1_COMPLEM", IIF(Type('jAddress["complemento"]') == "U", '', jAddress["complemento"]) })				

				IF Type('jAddress["bairro"]') == "U"
					::cErr := "[CCLIENTE] Propriedade nao informada: [bairro]"
					Return .F.
				EndIF
				Aadd(::aCliente, {"A1_BAIRRO", jAddress["bairro"]})	

				CC2->(DbSetOrder(4))
				If CC2->(DbSeek(xFilial('CC2') + jAddress["estado"] + PADR(FwNoAccent(UPPER(jAddress["cidade"])), LEN(CC2->CC2_MUN)) ))
					Aadd(::aCliente, {"A1_COD_MUN", CC2->CC2_CODMUN})	
				EndIf
				Aadd(::aCliente, {"A1_CODPAIS", "01058"})	
				Aadd(::aCliente, {"A1_INSCR", "ISENTO"})	
				Aadd(::aCliente, {"A1_TIPO", "F"})	
			Else
				xValue := &cField
                IF aStruct[nX][P_TP] == "D"
                    xValue := CToD(xValue)
                ElseIF aStruct[nX][P_TP] == "C"
                    xValue := AllTrim(xValue)
                EndIF	
            EndIF
			Aadd(::aCliente, {aStruct[nX][P_FD], xValue})
		Next nX	
		lRet := .T.				
	EndIF	

Return lRet

/*/{Protheus.doc} GetValue
	(long_description)
	@author Leonardo F. Bulcao - UserFunction
	@since 26/04/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method GetValue(cField, xRet) Class CCliente
		
	Local nP 		:= 0

	Default cField 	:= ""
	Default xRet	:= Nil

	IF Empty(::aCliente)
		::cErr	:= "[CCLIENTE] Dados de cadastro nao informados"
        Return .F.
	EndIF	

	IF (nP := Ascan(::aCliente, {|x| x[1] == cField})) > 0
		Return ::aCliente[nP][2]
	EndIF

Return xRet

/*/{Protheus.doc} Valida
	(long_description)
	@author Leonardo F. Bulcao - UserFunction
	@since 27/05/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method Valida() Class CCliente

	// Campos informados
	IF Empty(::cCGC)
		::cErr := "[CCLIENTE] CGC nao informado."
		Return .F.
	EndIF

	//
    DbSelectArea("SA1")
	SA1->(DbSetOrder(3))

	// Localiza
    IF SA1->(DbSeek(xFilial("SA1") + ::cCGC)) == .F.
		::cErr := "Cliente nao encontrado: [" + ::cCGC + "]"
        Return .F.
    EndIF

	::cCod 	:= SA1->A1_COD
	::cLoja	:= SA1->A1_LOJA
	
Return .T.

/*/{Protheus.doc} LoadStruct
	Montagem da estrutura
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Static Function LoadStruct(aStruct)

	Local lRet			:= .T.
	Local aAreaSX3		:= SX3->(GetArea())
	Local nX			:= 0
	
	Default aStruct		:= {}

	// Caso ja esteja populado
	IF Empty(_aStruct) == .F.
		aStruct := Aclone(_aStruct)
		Return .T.
	EndIF
	
	// Field Protheus, Propriedade Mobile, Tipo	
	Aadd(aStruct, {"A1_COD"		    , ""			, .F.})	
	Aadd(aStruct, {"A1_LOJA"	 	, ""			, .F.})
	Aadd(aStruct, {"A1_CGC"		    , "cpf"	, .T.})
	Aadd(aStruct, {"A1_PESSOA"		, ""			, .F.})		
	Aadd(aStruct, {"A1_NOME"		, "nome"	, .T.})	
	Aadd(aStruct, {"A1_DDD"		    , "telefone_celular"		, .F.})	
	Aadd(aStruct, {"A1_TEL"		    , "telefone_celular"		, .F.})	
	Aadd(aStruct, {"A1_EMAIL"		, "email"		, .F.})		
	Aadd(aStruct, {"A1_END"			, "endereco"		, .F.})	
			
	SX3->(DbSetOrder(2))
	For nX := 1 To Len(aStruct)
		IF SX3->(DbSeek(aStruct[nX][P_FD]))
			Aadd(aStruct[nX], SX3->X3_TIPO)
		EndIF
	Next nX

	SX3->(RestArea(aAreaSX3))

	//Salva na variavel estatica
	_aStruct := Aclone(aStruct)	

Return lRet
