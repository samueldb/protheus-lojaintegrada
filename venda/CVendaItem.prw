#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TRYEXCEPTION.CH"

#DEFINE ENTER CHR(13) + CHR(10)

#DEFINE P_FD 1	// Field
#DEFINE P_JS 2	// Json
#DEFINE P_OB 3	// Obrigatorio
#DEFINE P_TP 4	// Tipo

Static _aStruct := {}

/*/{Protheus.doc} CVendaItem
	Controle de itens
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	/*/
Class CVendaItem
		
	Data cErr	
	Data aVendaItem

	Data cPrdCod						// Codigo do produto
	Data cPrdTipo						// Tipo do produto
	Data cPrdUN							// Unidade
	Data cLocPad						// Local padrao
	Data cLocal							// Local
	Data cTES							// TES
	Data cCF 							// CFOP	
	Data cEntrega						// Entrega
	Data cTabPrc						// Tabela de preco
	Data nPrcTab						// Preco de tabela
						
	Method New() Constructor	
	Method FromJson(cVendaItem)
	Method GetValue(cField, xRet)
	Method LoadItem()					// Captura os dados do item para gravadao na SL2
	Method ValidEst(aItem)					// Captura os dados do item para gravadao na SL2
		
EndClass

/*/{Protheus.doc} New
	Construtor
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method New() Class CVendaItem	
				
	::cErr		:= ""	
	::aVendaItem	:= {}

	::cLocPad	:= ""
	::cLocal		:= ""
	::cTES		:= ""	
	::cCF		:= ""
	::cEntrega	:= ""	
	::cPrdCod	:= ""
	::nPrcTab	:= 0
	::cPrdTipo	:= ""
	::cPrdUN		:= ""	
	
Return

/*/{Protheus.doc} FromJson
	Conversao para objeto
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method FromJson(jObject) Class CVendaItem
		
	Local aStruct		:= {}	
	Local nX			:= 0
    Local nValDesc      := 0
	Local cPrdTeste		:= "2417624000002"
				
	Private jVendaItem  := JsonObject():New()
	Private cField		:= ""

	Default jObject  	:= ""

	// Efetua a conversao
	jVendaItem := jObject

	IF LoadStruct(@aStruct) == .F.
		::cErr := "[CVENDAITEM] Falha ao carregar a estrutura."
        Return .F.
	Else
		::aVendaItem := {}	
		For nX := 1 To Len(aStruct)
		
			cField 	:= ""
			cType 	:= ""
			xValue 	:= Nil

			IF Empty(aStruct[nX][P_JS]) == .F.
				cField 	:= 'jVendaItem["'+aStruct[nX][P_JS]+'"]'
				cType 	:= Type(cField)
				xValue 	:= &cField
				IF cType == "U" .And. aStruct[nX][P_OB]
					::cErr := "[CVENDAITEM] Campo obrigatorio nao informado: " + "[" + aStruct[nX][P_JS] + "]"
					Return .F.
				EndIF	
			EndIF		
			
			IF aStruct[nX][P_FD] == "L2_VRUNIT"
				nXiii := 1123
			EndIf

			IF aStruct[nX][P_TP] == "N" .AND. VALTYPE(xValue) == 'C'
				xValue := val(xValue)
			EndIf

			IF aStruct[nX][P_FD] == "L2_VALDESC"				
                xValue := ::GetValue("L2_PRCTAB", 0) - ::GetValue("L2_VRUNIT", 0)
            ElseIF aStruct[nX][P_FD] == "L2_DESC"				
                nValDesc := ::GetValue("L2_VALDESC")
                IF nValDesc > 0
                    xValue := (nValDesc / ::GetValue("L2_VRUNIT", 0) ) 
				Else
					xValue := 0  
                EndIF                       
            Else
                IF aStruct[nX][P_TP] == "D"
                    xValue := CToD(xValue)
                ElseIF aStruct[nX][P_TP] == "C"
                    xValue := AllTrim(xValue)
                EndIF	
            EndIF     
            Aadd(::aVendaItem, {aStruct[nX][P_FD], xValue})
		Next nX						
	EndIF	
	
	// If !(::ValidEst(::aVendaItem))
	// 	Return .F.
	// EndIf
Return .T.

/*/{Protheus.doc} GetValue
	(long_description)
	@author Leonardo F. Bulcao - UserFunction
	@since 26/04/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Method GetValue(cField, xRet) Class CVendaItem
		
	Local nP 		:= 0

	Default cField 	:= ""
	Default xRet	:= Nil

	IF Empty(::aVendaItem)
		::cErr	:= "[CVENDAITEM] Dados de cadastro nao informados"
        Return .F.
	EndIF	

	IF (nP := Ascan(::aVendaItem, {|x| x[1] == cField})) > 0
		Return ::aVendaItem[nP][2]
	EndIF

Return xRet

/*/{Protheus.doc} LoadStruct
	Montagem da estrutura
	@author Leonardo F. Bulcao - UserFunction
	@since 05/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	/*/
Static Function LoadStruct(aStruct)

	Local lRet			:= .T.
	Local aAreaSX3		:= SX3->(GetArea())
	Local nX			:= 0
	
	Default aStruct		:= {}

	// Caso ja esteja populado
	IF Empty(_aStruct) == .F.
		aStruct := Aclone(_aStruct)
		Return .T.
	EndIF
	
	// Field Protheus, Propriedade Mobile, Tipo	
	Aadd(aStruct, {"L2_PRODUTO"		, "sku"	, .T.})	
	Aadd(aStruct, {"L2_QUANT"		, "quantidade"	, .T.})	
    Aadd(aStruct, {"L2_VRUNIT"		, "preco_venda", .T.})
    Aadd(aStruct, {"L2_PRCTAB"		, "preco_venda"	, .T.})	
    Aadd(aStruct, {"L2_VALDESC"		, ""			, .F.})	 
	Aadd(aStruct, {"L2_DESC"		, ""			, .F.})	
	Aadd(aStruct, {"L2_VLRITEM"		, ""           	, .F.})		
	
	SX3->(DbSetOrder(2))
	For nX := 1 To Len(aStruct)
		IF SX3->(DbSeek(aStruct[nX][P_FD]))
			Aadd(aStruct[nX], SX3->X3_TIPO)
		EndIF
	Next nX

	SX3->(RestArea(aAreaSX3))

	//Salva na variavel estatica
	_aStruct := Aclone(aStruct)	

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidEst
	(long_description)
	@type  Static Function
	@author Samuel Dantas
	@since   10/03/2022
	@version 1.0
/*/
//-------------------------------------------------------------------
Method ValidEst(aItem) Class CVendaItem
	Local aAreaSB2 	:= SB2->(GetArea())
	Local cProduto 	:= ''
	Local nSldSB2 	:= 0
	Local nQtd 	:= 0
	Local nPosProd := 0
	Local nPosQtd := 0
	Local lRet := .T.
	nPosProd 	:= aScan(aItem,{|x| AllTrim(x[1]) == 'L2_PRODUTO'})
	nPosQtd 	:= aScan(aItem,{|x| AllTrim(x[1]) == 'L2_QUANT'})
	
	cProduto 	:= aItem[nPosProd][2]
	nQtd 		:= aItem[nPosQtd][2]
	
	SB2->(DbSetOrder(1))
	If SB2->(DbSeek(xFilial('SB2') + PADR(cProduto, len(SB1->B1_COD))))
		nSldSB2 := SaldoSb2()
		If nSldSB2 < 1//nQtd
			lRet := .F.
			::cErr := "[CVENDAITEM] Produto '"+cProduto+"' n�o tem estoque suficiente para esta solicita��o."
		EndIf
	EndIf
	
	SB2->(RestArea(aAreaSB2))
Return lRet
